% inter-frame difference analysis
function decision = ifd(Imwork, Imback, figdiff)

[MR,MC,Dim] = size(Imback);

decision = (abs(Imwork(:,:,1)-Imback(:,:,1)) > 20) ...
   | (abs(Imwork(:,:,2) - Imback(:,:,2)) > 20) ...
   | (abs(Imwork(:,:,3) - Imback(:,:,3)) > 20);

if figdiff > 0
  figure(figdiff)
  imshow(decision)
end

return
