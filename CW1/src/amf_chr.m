% Median analysis
function [decision, history] = amf_chr(Imwork, history, figdiff)

[MR,MC,Dim,old] = size(history);
decision = zeros(MR,MC);
Imchr = chromaticity(Imwork);

medians = median(history,4);

decision = (abs(Imchr(:,:,1) - medians(:,:,1)) > 10) ...
         | (abs(Imchr(:,:,2) - medians(:,:,2)) > 10) ...
         | (Imchr(:,:,3)./medians(:,:,3) < 0.8) ...
         | (Imchr(:,:,3)./medians(:,:,3) > 1.2);

% update history
for i = 1 : old-1
  history(:,:,:,i) = history(:,:,:,i+1);
end
history(:,:,:,old) = Imchr(:,:,:);

if figdiff > 0
  figure(figdiff)
  imshow(decision)
end

return
