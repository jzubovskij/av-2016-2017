% Median analysis
function [decision, history] = amf(Imwork, history, figdiff)

[MR,MC,Dim,old] = size(history);
decision = zeros(MR,MC);

medians = mean(history,4);

decision = (abs(Imwork(:,:,1) - medians(:,:,1)) > 15) ...
         | (abs(Imwork(:,:,2) - medians(:,:,2)) > 15) ...
         | (abs(Imwork(:,:,3) - medians(:,:,3)) > 15);

for i = 1 : old-1
  history(:,:,:,i) = history(:,:,:,i+1);
end
history(:,:,:,old) = Imwork(:,:,:);

if figdiff > 0
  figure(figdiff)
  imshow(decision)
end

return
