% converts an image to its chromaticity equivalent
function Imchr = chromaticity(Im)

[MR,MC,Dim] = size(Im);
Imchr = zeros(MR,MC,Dim);

for i = 1 : MR
  for j = 1 : MC
    sum = Im(i,j,1) + Im(i,j,2) + Im(i,j,3);
    Imchr(i,j,3) = sum/3;
    if sum == 0
      sum = 1;
    end
    Imchr(i,j,1) = Im(i,j,1)/sum;
    Imchr(i,j,2) = Im(i,j,2)/sum;
  end
end
return;

