% RGA analysis
function [decision, mean, var] = rga(Imwork, mean, var, figdiff)

[MR,MC,Dim] = size(Imwork);
decision = zeros(MR,MC);
ro = 0.01;
k = 2.5;

diff = abs(Imwork(:,:,:)-mean(:,:,:));

decision = ((diff(:,:,1)./var(:,:,1)) > k) ...
         | ((diff(:,:,2)./var(:,:,2)) > k) ...
         | ((diff(:,:,3)./var(:,:,3)) > k);

% update mean and variance
for i = 1 : 3
  mean(:,:,i) = (1-decision(:,:)) .* (ro*Imwork(:,:,i) + (1-ro)*mean(:,:,i))    + (decision(:,:)) .* mean(:,:,i);
end
var(:,:,:) =  ro*diff(:,:,:).^2 + (1-ro)*var(:,:,:);

if figdiff > 0
  figure(figdiff)
  imshow(decision)
end

return
