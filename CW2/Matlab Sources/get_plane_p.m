% Get plane vector
function [plane_p] = get_plane_p(pointlist)

  [L,~] = size(pointlist);  % input list of L points
  plane_p = zeros(4,1);
  D = zeros(L,4);           % working array

  com = mean(pointlist);    % compute centre of mass
  for k = 1 : L
    % subtract centre of mass to improve numerics
    % use a scale factor of 100 internally to balance value sizes
    D(k,:) = [pointlist(k,1)-com(1),pointlist(k,2)-com(2),pointlist(k,3)-com(3),100];
  end
  S = D'*D;                 % compute scatter matrix
  [~,~,V] = svd(S);         % find eigenvectors
  N = V(1:3,4);             % plane normal is eivenvector with smallest eigenvalue
  
  plane_p(1:3) = N' / norm(N); % make to unit normal
  plane_p(4) = 100*V(4,4) / norm(N) - com*plane_p(1:3); % recompute d in N*x+d=0 fit

end
