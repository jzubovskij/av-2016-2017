%Function gets the motion of a selected patch between two images using SURF
%feature matching
function [motion,scale] = surf_matcher(filename1, filename2, image_number)

global X_BOT Y_BOT X_DIM Y_DIM
global IMAGE_Y_DIM IMAGE_X_DIM
global PLOT_SURF

%get the first picture
load(filename1,'Img');
img1 = Img( (Y_BOT:Y_BOT + Y_DIM) , (X_BOT:X_BOT+X_DIM));

%get the second picture
load(filename2,'Img');
img2 = Img(1:IMAGE_Y_DIM, 1:IMAGE_X_DIM);

%detect the features
points1 = detectSURFFeatures(img1,'MetricThreshold', 100);
points2 = detectSURFFeatures(img2, 'MetricThreshold', 100);

%extract the features
[f1,vpts1] = extractFeatures(img1,points1);
[f2,vpts2] = extractFeatures(img2,points2);

%match features by using their description
indexPairs = matchFeatures(f1,f2, 'Method', 'Exhaustive',...
    'MatchThreshold', 1.0,'MaxRatio', 0.6) ;
%get location of corresponding points from both images
matchedPoints1 = vpts1(indexPairs(:,1));
matchedPoints2 = vpts2(indexPairs(:,2));

%estimate transformation
[tform, inlierNew, inlierOriginal] = estimateGeometricTransform(...
    matchedPoints2, matchedPoints1, 'projective', 'Confidence', 90);

if PLOT_SURF
    %plot matches
    figure(image_number*10 + 7);
    showMatchedFeatures(img1,img2, inlierOriginal, inlierNew);
    title('Matching points (inliers only)');
    legend('Original Points','New Points');
end

%decode the transform into translation, rotation and scale components
Tinv  = tform.invert.T;
ss = Tinv(2,1);
sc = Tinv(1,1);
%get the scaling
scale = sqrt(ss*ss + sc*sc);
theta = atan2(ss,sc)*180/pi;
tx  = Tinv(3,1);
ty  = Tinv(3,2);

%get transaltion
motion = [double(tx),double(ty)];
