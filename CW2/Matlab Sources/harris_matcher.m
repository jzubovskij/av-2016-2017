%Funtion returns movement estimation between to images of specified patch
function [img1, img2,Loc] = harris_matcher(filename_1, filename_2, image_number)

global X_BOT Y_BOT X_DIM Y_DIM SEARCH_DIST PLOT_HARRIS

load(filename_1,'Img');
img1 = Img( (Y_BOT:Y_BOT + Y_DIM) , (X_BOT:X_BOT+X_DIM));

load(filename_2,'Img');
img2 = Img( (1:Y_BOT + Y_DIM+SEARCH_DIST),...
    (1:X_BOT+X_DIM+SEARCH_DIST));


ptsOriginal  = detectHarrisFeatures(img1);
ptsSub = detectHarrisFeatures(img2);
% Extract feature descriptors.

[featuresOriginal,   validPtsOriginal]  = extractFeatures(img1,  ptsOriginal);
[featuresDistorted, validptsSub]  = extractFeatures(img2, ptsSub);
% Match features by using their descriptors.

indexPairs = matchFeatures(featuresOriginal, featuresDistorted);
% Retrieve locations of corresponding points for each image.

matchedOriginal  = validPtsOriginal(indexPairs(:,1));
matchSub = validptsSub(indexPairs(:,2));
% Show point matches. Notice the presence of outliers.

if PLOT_HARRIS
    figure(image_number*10 + 7);
    showMatchedFeatures(img1,img2,matchedOriginal,matchSub);
    title('Putatively matched points (including outliers)');
end


% Step 4: Estimate Transformation
% Find a transformation corresponding to the matching point pairs using the 
%statistically robust M-estimator SAmple Consensus (MSAC) algorithm, which 
%is a variant of the RANSAC algorithm. It removes outliers while computing 
%the transformation matrix.

[tform, inlierDistorted, inlierOriginal] = estimateGeometricTransform(...
    matchSub, matchedOriginal, 'affine','MaxNumTrials',3000);

Tinv  = tform.invert.T;
tx = Tinv (3,1:2);
ss = Tinv(2,1);
sc = Tinv(1,1);
scale_recovered = sqrt(ss*ss + sc*sc);
theta_recovered = atan2(ss,sc)*180/pi;

Loc = [int16(tx(1)) ,Y_BOT + int16(tx(2))];