% Plot a linear array of points as a plane in specified colour on the
% specified figure
function plot_plane(points, colour, figure_number)

[N,~] = size(points);

%if a single point, do not plot
if N < 2
    sprintf('No points to plot')
    return;
end    

%get axis independently
Xs = points(:,1);
Ys = points(:,2);
Zs = points(:,3);


figure(figure_number);
hold on

%plot a set of unconnected points
plot3(Xs,Ys,Zs,'Color',colour, 'linestyle', 'none', 'marker', '.');

%set viewpoint
az = -97;
el = 10;
view(az, el);

hold off

end

