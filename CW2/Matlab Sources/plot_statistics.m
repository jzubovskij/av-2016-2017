%function plots all statistics related to dataset
function plot_statistics(points, points_2d, mode, image_number)

%mode = 1 means average x
%mode = 2 means average y
%mode = 3 means average z (so does 4)

global FILENAMES PATCH_SIZES OUTLIERS RMSS 
global PRINT

%figure to draw RMS on
RMS_VS_VALUE_FIG = 1;
%e_5 vs imgae number figure, also occupies the +1 one for the 2D version
E_5_FIG = 2; 
%figure to draw residuals on
RESIDUALS_FIG = 9;

%get average coordinate
[value, axis] = get_avg_coordinate(points, mode);
%get plane parameter
[plane_p] = get_plane_p(points);
%get rms, residuals and residuals > 5*RMS
[rms, rms_2d, e, e_2d, e_5, ~] = get_rms(plane_p, points, points_2d);

%plot residuals
plot_residuals(e, e_2d, rms, rms_2d, RESIDUALS_FIG + 10*image_number);

if PRINT
    sprintf('The number e > 5*rms is %d for image %d', e_5,image_number)
end

%add the e_5 to the array
OUTLIERS(image_number) = cellstr(sprintf('%d', e_5));
%add the RMS value to the array
RMSS(image_number) = cellstr(sprintf('%d', rms));

%row names for the table
image_numbers = [{'image 1'},{'image 2'},{'image 3'},{'image 4'},...
    {'image 5'},{'image 6'},{'image 7'},{'image 8'},...
    {'image 9'},{'image 10'},{'image 11'}];

%plot table with e_5 vs. image number for current dataset
figure(E_5_FIG)
T = table(PATCH_SIZES, OUTLIERS, RMSS, 'RowNames', FILENAMES,...
        'VariableName', {'Patch_Size', 'Outliers', 'RMS'});
uitable('Data',T{:,:},'ColumnName',T.Properties.VariableNames,...
    'RowName',T.Properties.RowNames,'Units', 'Normalized', 'Position',[0, 0, 1, 1]);

%plot rms vs avg value
plot_rms_vs_value(rms, value, RMS_VS_VALUE_FIG, axis);

end

