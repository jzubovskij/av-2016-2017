%plots the residuals vector in colour as per specifications
function plot_residuals(e, e_2d, rms, rms_2d, figure_number)

global X_DIM Y_DIM

%make an image
img = zeros(Y_DIM,X_DIM,3);

for x = 1:X_DIM
    for y = 1:Y_DIM
        
        %as linear traversal during linearization was X, then Y
        i = (x-1)*Y_DIM + y;
        
        if e(i) < -rms
            %then colour is red
            colour = [255,0,0];
        elseif (e(i) >= -rms) && (e(i) < 0.0)
            %then colour is yellow
            colour = [255,255,0];
        elseif (e(i) < rms) && (e(i) >= 0.0)
            %then colour is green
            colour = [0,255,0];
        elseif e(i) >= rms
            %then colour is blue
            colour = [0,0,255];
        end
        
        
        img(y, x, :) = colour;
        
    end
end

figure(figure_number);
hold on

axis('image');

title('Residuals')
% title
xlabel('X')
% x-axis label
ylabel('Y')
% y-axis label
image(img);
box off

%view it from above
az = 0;
el = 90;
view(az, el);
hold off

end

