function plot_patch(filename, file_number, offset)
% plots currently analyzed patch, shows its centre
% and outline
global X_BOT Y_BOT X_DIM Y_DIM PLOT_BIG_IMAGE

load(filename,'Img');
f1 = Img;

hold on
%set size to be larger
hFig = figure(10 * file_number + 1);
set(hFig, 'Position', [0 0 1200 1200])

%plot patch on the subplot
subplot(2,2,4);
imshow(f1);
title('Image')
xlabel('X')
ylabel('Y')



%indicate patch being analyzed via red rectangle
rectangle('Position',[X_BOT Y_BOT X_DIM Y_DIM],'EdgeColor','r','LineWidth',3);
viscircles([(X_BOT+X_DIM/2) (Y_BOT+Y_DIM/2)],[3]);
hold off


if PLOT_BIG_IMAGE
    
    hold on
    figure(10 * file_number )
    
    %plot patch on the subplot
    title('X Data')
    xlabel('X')
    ylabel('Y')
    
    imshow(f1);
    
    %indicate patch being analyzed via red rectangle
    rectangle('Position',[X_BOT Y_BOT X_DIM Y_DIM],'EdgeColor','r','LineWidth',3);
    viscircles([(X_BOT+X_DIM/2) (Y_BOT+Y_DIM/2)],[3]);
    hold off
    
end

end

