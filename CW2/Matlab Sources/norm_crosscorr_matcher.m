%Function returns the estimated movement of a selected patch between images
function [img1, img2,Loc] = norm_crosscorr_matcher(filename_1, filename_2, image_number)

global X_BOT Y_BOT X_DIM Y_DIM PLOT_X_CORR
% Load image1
load(filename_1,'Img');
img1 = Img((Y_BOT:Y_BOT + Y_DIM) , (X_BOT:X_BOT+X_DIM));
% Load image2
load(filename_2,'Img');
Img(find(~isfinite(Img))) = 0;
img2 = Img;

% Calculate the normalized cross correlation coefficient

c = normxcorr2(img1,img2);
if PLOT_X_CORR
	% Visualize the cross-correlation
    figure(image_number*10 + 8), surf(c), shading flat;
end
% Locate the larges cross-correlation in the image
[ypeak, xpeak] = find(c==max(c(:)));
% subract the original dimension of the image 
% this is because we need the beginning of the image
% DIM/2 is the over-hang due to matching and we also need to subtract 
% the distance from the center to the edge of the patch
 
yoffSet = ypeak-size(img1,1);
xoffSet = xpeak-size(img1,2);
if PLOT_X_CORR
    hFig = figure(image_number*10 + 9);
    hAx  = axes;
    imshow(img2,'Parent', hAx);
    imrect(hAx, [yoffSet+1, xoffSet+1, size(img1,1), size(img1,2)]);
end
Loc = [xoffSet,yoffSet];






