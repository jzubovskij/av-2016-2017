%Does plane analysis on the file with given file name, of the given
%sequence number in the sequence of images being analyzed.
%Mode parameter specifies patch movement axis
function error = process_image(filename, image_number, mode)

%global variables to show what we display in addition to main stats
global PLOT_POINTS PLOT_MESH PLOT_XYZ

%get the image data from the file
[RGBXYZ, R, XYZ_2D, error] = load_pcl_mat(filename);

%if plotting the X,Y,Z
if PLOT_XYZ
    
    
    figure(10 * image_number + 1)
    hold on
    colormap('Gray')
    
    %plot X
    
    subplot(2,2,1);
    imagesc(XYZ_2D(:,:,1));
    title('X Data')
    xlabel('X')
    ylabel('Y')

    %plot Y
    subplot(2,2,2);
    imagesc(XYZ_2D(:,:,2));
    title('Y Data')
    xlabel('X')
    ylabel('Y')
    
    %plot Y
    subplot(2,2,3);
    imagesc(XYZ_2D(:,:,3));
    title('Z Data')
    xlabel('X')
    ylabel('Y')
    
    hold off
    
end


%if indices specified out of bounds of image size, exit
if (error == 1)
    error = 1;
    return;
end

%plot the texture onto the 3D structure
if PLOT_MESH
    plotpcl_mesh(RGBXYZ, image_number* 10 + 4);
end

%mono coloured 3D image of the point cloud
if PLOT_POINTS
    figure(image_number * 10 + 5)
    plot3(R(:,1),R(:,2),R(:,3),'k.')
    pause(1)
end


%plot the plane analysis statistics
plot_statistics(R, XYZ_2D, mode, image_number);

return

end

