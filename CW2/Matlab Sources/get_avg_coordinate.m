% Return the average coordinate value
function [avg_value, axis] = get_avg_coordinate(points, dimension)

global X_BOT Y_BOT X_DIM Y_DIM

%get average x
if dimension == 1
    avg_value =  X_BOT+ X_DIM/2;
    axis = 'X';
    
elseif dimension == 2
    %get average y
    avg_value =   Y_BOT + Y_DIM/2;
    axis = 'Y';
    
else
    %get average z (for zmove or zstatic)
    values = points(:,3);
    avg_value = mean(values);
    axis = 'Z';
end

end

