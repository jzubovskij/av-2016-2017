% Plot set of points as a random colour, allowing many planes to be plotted
% with different colours (same colour ocurring has very low probability)
function plot_identified_plane(points, plane_number, figure_number)

%all possible colours combinations
colours = [[0 0 0]; [0 0 1]; [0 1 0]; [0 1 1];...
    [1 0 0]; [1 0 1]; [1 1 1]; [1 1 1]];

%get random colour
colour_number = mod(plane_number, 8) + 1;
colour = colours(colour_number, :);

%get random colour gradient
colour_gradients = [ rand rand rand ];

%make new scaled colour
resulting_colour = colour .* colour_gradients;

%plot plane in said (highly probably) unique colour
plot_plane(points, resulting_colour, figure_number);


end

