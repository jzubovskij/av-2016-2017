% Does all the actions relating to plane analysis of alll images in the
% specified folder, analysis is done according to movement axis
function doall(folder, move_axis, matcher)

%remove all previous graphs from the scren
kill;

%global variables
global DEBUG
%analyzed patch positions
global X_BOT Y_BOT X_DIM Y_DIM
%dimensions of analyzed image
global IMAGE_X_DIM IMAGE_Y_DIM
%global and patch selection point limits
global STOP_LIMIT PATCH_LIMIT
%limits on how stuff fits onto the plane
global DISTTOL_LIMIT PLANETOL_LIMIT
%grow point limits
global GROW_LIMIT GROW_INCREMENT
%eror coefficients
global GROW_ERROR PATCH_ERROR
%search distance for meovement estimation
global SEARCH_DIST
%printing of the patch movement results and analysis
global PLOT_PATCHES PLOT_POINTS PLOT_MESH
%detailed printing of patch movement analysis
global PLOT_X_CORR PLOT_HARRIS PLOT_SURF
%print debug messages
global PRINT
%vector to hold data for the data table
global OUTLIERS RMSS
%the global array of file names
global FILENAMES
%global array of patch sizes
global PATCH_SIZES
%global variable to plot X,Y,Z
global PLOT_XYZ
global PLOT_BIG_IMAGE



DEBUG         = 0;
PLOT_PATCHES  = 1;
PLOT_POINTS   = 0;
PLOT_MESH     = 0;
PLOT_X_CORR   = 0;
PLOT_HARRIS   = 0;
PLOT_SURF     = 0;
PRINT         = 0;
PLOT_XYZ      = 1;
PLOT_BIG_IMAGE = 0;


%initialize the arrays
OUTLIERS = {'1';'2';'3';'4';'5';'6';'7';'8';'9';'10';'11'};
RMSS = {'1';'2';'3';'4';'5';'6';'7';'8';'9';'10';'11'};
FILENAMES = {'1';'2';'3';'4';'5';'6';'7';'8';'9';'10';'11'};
PATCH_SIZES = {'1';'2';'3';'4';'5';'6';'7';'8';'9';'10';'11'};

% if moving along x_axis (xmove) for maximum patch seen in all frames
if move_axis == 1
    X_BOT = 50;
    Y_BOT = 250;
    X_DIM = 120;
    Y_DIM = 800;
end

%if moving along y axis (ymove) for maximum patch seen in all frames
if move_axis == 2
    X_BOT = 100;
    Y_BOT = 50;
    X_DIM = 800;
    Y_DIM = 120;
end


%if moving along z axis (zmove) for maximum patch seen in all frames
if move_axis == 3
    X_BOT = 350;
    Y_BOT = 50;
    X_DIM = 500;
    Y_DIM = 500;
end

%if zstatic for maximum patch seen in all frames
if move_axis == 4
    X_BOT = 400;
    Y_BOT = 50;
    X_DIM = 500;
    Y_DIM = 500;
end


SEARCH_DIST     = 100;
STOP_LIMIT      = X_DIM * Y_DIM / 100;
PATCH_LIMIT     = X_DIM * Y_DIM;
DISTTOL_LIMIT   = 0.01;
GROW_LIMIT      = X_DIM * Y_DIM;
GROW_INCREMENT  = X_DIM * Y_DIM / 50;
PATCH_ERROR     = 0.1;
GROW_ERROR      = 0.04;
PLANETOL_LIMIT  = 0.01;

%get files in subdirectory for specified dataset
directory = sprintf('%s%s', folder, '/*.mat');
files = dir(directory);
[file_count, ~] = size(files);

%identify that no files found in indicated folder
if file_count < 1
    sprintf('No files found in directory:  %s', directory)
    return;
end

for file_number = 1 : file_count
    
    pause(1);
    
    file1 = [folder '/' files(file_number).name];
    
    %assign values to global arrays
    FILENAMES(file_number) = cellstr(files(file_number).name);
    PATCH_SIZES(file_number) = cellstr(sprintf('%d x %d ', X_DIM, Y_DIM));

    
    %plot currently analyzed patch
    if PLOT_PATCHES
        plot_patch(file1, file_number, 0);
    end
    
    
    %re-select patch if lost all initail points
    if file_number == 7 && move_axis == 2
            %select a Y away from upper edge
            Y_BOT = IMAGE_Y_DIM * 0.1;
            
            %reproces the data
            file1 = [folder '/' files(file_number).name];
            %attemopt to reprocess it
            process_image(file1, file_number, move_axis);
    end
    %get analysis statistics
    process_image(file1, file_number, move_axis);
    
    %do not try to match to the next file if no more files left
    if file_number == file_count
        return;
    end
    
    file2 = [folder '/' files(file_number+1).name];

    if matcher == 1
        [motion,scale] = surf_matcher(file1, file2, file_number);
        %if using surf, also scale it
        X_DIM = round(X_DIM * scale);
        Y_DIM = round(Y_DIM * scale);
        
    elseif matcher == 2
            
        % Calculate the normalized cross correlation of the patch with the whole of
        % the next image and selects the peak.
        [~, ~ ,motion] = norm_crosscorr_matcher(file1, file2, file_number);
        
    else
        %use the harris fature matcher
        [~, ~ ,motion] = harris_matcher(file1, file2, file_number);
    end
    
    %update patch location
    X_BOT = round(motion(1));
    Y_BOT = round(motion(2));
        
end



