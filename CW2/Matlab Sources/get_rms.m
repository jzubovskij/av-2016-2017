%calculate RMS of plane vs. points used to make it
function [rms, rms_2d, e, e_2d, e_5, e_5_2d] = get_rms(plane_p, points, points_2d)

global X_DIM Y_DIM

%get normalized plane normal
plane_q = plane_p / norm(plane_p(1:3));

%Get them from the linear array of points
[N, ~] = size(points);

%make augmented linear point array
points_aug = zeros(N,4);
points_aug(:, 1:3) = points(:, 1:3);
points_aug(:, 4)   = 1;

e = zeros(N,1);

%calculate residuals
for i = 1:N
    e(i) = dot(plane_q, points_aug(i, :));
end

%get rms
rms = sqrt(sum(e.^2) / N);

%get e vector > 5*rms and gets the size
[e_5, ~] = size((find(abs(e) > 5*rms)));

%make a 2d augmented matrix
points_aug_2d = zeros(Y_DIM,X_DIM,4);
points_aug_2d(:,:, 1:3) = points_2d(:,:, 1:3);
points_aug_2d(:,:, 4)   = 1;


e_2d = zeros(Y_DIM,X_DIM,1);

%calculate the residuals
for y = 1:Y_DIM-1
    for x = 1:X_DIM-1
        
        point = zeros(4,1);
        point(:) = points_aug_2d(y,x,:);
        
        e_2d(y,x) = dot(plane_q, point);
    end
end

%intermediate array
temp = e_2d.^2;
%get rms from a 2d matrix
rms_2d = sqrt(sum(temp(:)) / double(X_DIM*Y_DIM));
%find all elements in the e_2d matrix > 5*rms_2d and gets the size
[e_5_2d, ~] = size(find(abs(e_2d) > 5*rms_2d));



end

