% Plot the RMS vs average value
function plot_rms_vs_value(rms, value, figure_number, axis)

%get size of arrays
figure(figure_number)
hold on
title(['RMS vs Average ', axis])  % title
xlabel(['Average ', axis]) % x-axis label
ylabel('RMS') % y-axis label
stem(value, rms,'filled',':diamondr');
hold off



end

