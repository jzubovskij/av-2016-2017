%Returns a 2D and linearized arrays of both point
%positions and colour values
function [RGBXYZ, XYZ_linear, XYZ_2D, error] = load_pcl_mat(filename)

global X_BOT Y_BOT X_DIM Y_DIM
global IMAGE_X_DIM IMAGE_Y_DIM 

load(filename);
[m,n,~] = size(XYZ);

error = 0;

%get the top coordinate for the patch
X_TOP = X_BOT + X_DIM-1;
Y_TOP = Y_BOT + Y_DIM-1;

%initialize the arrays
RGBXYZ = zeros(Y_DIM,X_DIM,6);
XYZ_linear = zeros(Y_DIM*X_DIM, 3);
XYZ_2D = zeros(Y_DIM, X_DIM, 3);

IMAGE_X_DIM = m;  
IMAGE_Y_DIM = n;

%check if out of bounds
if (X_TOP > m || Y_TOP > n)
    error = 1;
    ['**************** EXCEEDING DIMENSIONS']
    return
end

%get the 2D arrays for each axis separately
X = XYZ(Y_BOT:Y_TOP, X_BOT:X_TOP,1);
Y = XYZ(Y_BOT:Y_TOP, X_BOT:X_TOP,2);
Z = XYZ(Y_BOT:Y_TOP, X_BOT:X_TOP,3);

%get the 2D array for the axis
XYZ_2D = XYZ(Y_BOT:Y_TOP, X_BOT:X_TOP, :);

%replace all the invalid points with zeros
K = isnan(X) | isinf(X) | isnan(Y) | isinf(Y) | isnan(Z) | isinf(Z);
X(K) = 0;
Y(K) = 0;
Z(K) = 0;
Img = 255*cat(3,Img,Img,Img);

%put the values back in
RGBXYZ(:,:,4) = X;
RGBXYZ(:,:,5) = Y;
RGBXYZ(:,:,6) = Z;

%put RGB values into the array
RGBXYZ(:,:,1) = Img(Y_BOT:Y_TOP,X_BOT:X_TOP,1);
RGBXYZ(:,:,2) = Img(Y_BOT:Y_TOP,X_BOT:X_TOP,2);
RGBXYZ(:,:,3) = Img(Y_BOT:Y_TOP,X_BOT:X_TOP,3);

%get the linear array for the axis
a = reshape(X, X_DIM*Y_DIM, 1);
b = reshape(Y, X_DIM*Y_DIM, 1);
c = reshape(Z, X_DIM*Y_DIM, 1);
XYZ_linear = cat(2, a, b ,c);

end
